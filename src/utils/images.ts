const normalizeImages = (images: string | string[]): string[] => {
  const isValidJson = (str: string): boolean => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  const cleanString = (str: string): string => {
    return str.replace(/^\["|"\]$/g, '').replace(/^"|"$/g, '');
  };

  if (typeof images === 'string') {
    if (isValidJson(images)) {
      const parsedImages = JSON.parse(images);
      return Array.isArray(parsedImages)
        ? parsedImages.flatMap((image) => normalizeImages(image))
        : [parsedImages];
    } else {
      return [images];
    }
  } else {
    return images.flatMap((image) => {
      if (typeof image === 'string') {
        if (isValidJson(image)) {
          return normalizeImages(image);
        } else {
          return cleanString(image);
        }
      }
      return image;
    });
  }
};

export { normalizeImages };
