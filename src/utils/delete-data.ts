import { ErrorWithStatus, nvp } from '@/libs/fetch';

interface DeleteOption {
  url: string;
}

interface DataReturn {
  success?: boolean;
  error?: { status: number; message: string };
}

const deleteData = async ({ url }: DeleteOption): Promise<DataReturn> => {
  try {
    await nvp.delete(url);

    return {
      success: true,
    };
  } catch (error) {
    const err = error as ErrorWithStatus;

    return {
      error: { status: err.status, message: err.error },
    };
  }
};

export { deleteData };
