/* eslint-disable no-new */
import { appConfig } from '@/app.config';

export const formatByteToKb = (byte: number): string => {
  const sizeInKb = byte / 1024;
  return `${sizeInKb.toFixed(2)}Kb`;
};

export const removeLocalePrefix = (url: string) => {
  const { locales } = appConfig.i18n;
  const localePattern = locales.join('|');
  const pattern = new RegExp(`^/(${localePattern})(/|$)`);

  return url.replace(pattern, '/');
};

export const isValidUrl = (string: string): boolean => {
  try {
    return Boolean(new URL(string));
  } catch {
    return false;
  }
};
