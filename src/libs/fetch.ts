import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';

export interface FetchOpt {
  revalidate?: false | number;
  cache?: 'force-cache' | 'no-store';
  tags?: string[];
  headers?: Record<string, string>;
  signal?: AbortSignal;
}

interface ResponseData {
  message?: string;
  error?: string;
}

export type FetchOptWithBody = FetchOpt & { body?: any };

export type ErrorWithStatus = Error & { status: number; error: string };

export class BtError extends Error {
  status: number;

  error: string;

  constructor(message: string, status: number, error: string) {
    super(message);

    this.status = status;
    this.error = error;
  }
}

const isFormData = (body: any): body is FormData => body instanceof FormData;

/**
 * Note:
 * revalidate: 0 === cache: 'no-store'
 * revalidate: >0 === cache: 'force-cache'
 * revalidate: false === revalidate: Infinity === cache: 'force-cache'
 */

const f = async (
  url: string,
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH',
  {
    revalidate,
    cache = 'no-store',
    body,
    headers = {},
    signal,
  }: FetchOptWithBody = {},
) => {
  try {
    const option: AxiosRequestConfig = {
      url,
      method,
      headers: {
        ...(isFormData(body) ? {} : { 'Content-Type': 'application/json' }),
        ...headers,
      },
      data: isFormData(body) ? body : body ? JSON.stringify(body) : undefined,
      signal,
    };

    // Initialize headers if undefined
    option.headers = option.headers || {};

    if (revalidate !== undefined) {
      // Handle revalidate logic if needed
    } else {
      option.headers['Cache-Control'] =
        cache === 'no-store' ? 'no-store' : 'force-cache';
    }

    const response: AxiosResponse = await axios(option);

    return response.data;
  } catch (err) {
    const error = err as AxiosError;
    const responseData: ResponseData = error.response?.data || {};
    const message = responseData.message || error.message || 'Unknown error';
    const status = error.response?.status || 500;
    const errorData = responseData.error || 'Internal server error';

    throw new BtError(message, status, errorData);
  }
};

export const nvp = {
  get: (url: string, option?: FetchOpt) => f(url, 'GET', option),
  post: (url: string, option?: FetchOptWithBody) => f(url, 'POST', option),
  put: (url: string, option?: FetchOptWithBody) => f(url, 'PUT', option),
  delete: (url: string, option?: FetchOpt) => f(url, 'DELETE', option),
  patch: (url: string, option?: FetchOptWithBody) => f(url, 'PATCH', option),
};
