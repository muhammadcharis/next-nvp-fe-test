import { Product } from '@/app/[locale]/(root)/products/_schemas/product';
import { PayloadAction, createSlice } from '@reduxjs/toolkit';

interface CartState {
  items: Product[];
}

const initialState: CartState = {
  items: [],
};

// Create a Redux slice for managing card data
const cartSlice = createSlice({
  name: 'cart', // Name of the slice
  initialState, // Initial state
  reducers: {
    addCart(state, action) {
      const newItem = action.payload;
      const existingItem = state.items.find((item) => item.id === newItem.id);

      if (!existingItem) {
        state.items.push(newItem);
      }
    },
    removeCart(state, action: PayloadAction<number>) {
      const id = action.payload;
      const existingItem = state.items.find((item) => item.id === id);
      if (existingItem) {
        state.items = state.items.filter((item) => item.id !== id);
      }
    },
  },
});

export const { addCart, removeCart } = cartSlice.actions;
export default cartSlice.reducer;
