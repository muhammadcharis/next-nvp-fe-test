import { Product } from '@/app/[locale]/(root)/products/_schemas/product';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ProductState {
  limit: number;
  offset: number;
  hasModal: 'delete' | 'create' | 'edit' | null;
  selectedProduct: Product | null;
}

const initialState: ProductState = {
  limit: 5,
  offset: 0,
  hasModal: null,
  selectedProduct: null,
};

// Create a Redux slice for managing products data
const productSlice = createSlice({
  name: 'product', // Name of the slice
  initialState, // Initial state
  reducers: {
    setLimit(state, action: PayloadAction<number>) {
      state.limit = action.payload;
    },
    setOffset(state, action: PayloadAction<number>) {
      state.offset = action.payload;
    },
    setHasModal(
      state,
      action: PayloadAction<'delete' | 'create' | 'edit' | null>,
    ) {
      state.hasModal = action.payload;
    },
    setSelectedProduct(state, action: PayloadAction<Product | null>) {
      state.selectedProduct = action.payload;
    },
  },
});

export const { setLimit, setOffset, setHasModal, setSelectedProduct } =
  productSlice.actions;
export default productSlice.reducer;
