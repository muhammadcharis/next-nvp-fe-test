export interface ProductResponse {
  id?: number;
  category?: {
    id: number;
    name: string;
  };
  description?: string;
  images?: string[];
  price?: number;
  title?: string;
}
