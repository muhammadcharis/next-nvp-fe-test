import { NextRequest, NextResponse } from 'next/server';
import { env } from '@/env.mjs';
import { ErrorWithStatus, nvp } from '@/libs/fetch';
import { createResponseError } from '@/utils/route';

const GET = async (req: NextRequest) => {
  try {
    const { API_URL } = env;

    const { searchParams } = req.nextUrl;
    const limit = searchParams.get('limit');
    const offset = searchParams.get('offset');
    const keyword = searchParams.get('keyword');

    const apiUrl = new URL(`${API_URL}/products`);
    if (limit) apiUrl.searchParams.append('limit', limit.toString());
    if (offset) apiUrl.searchParams.append('offset', offset.toString());
    if (keyword) apiUrl.searchParams.append('keyword', keyword);

    const data = await nvp.get(apiUrl.toString());

    return NextResponse.json(data);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

const POST = async (req: NextRequest) => {
  try {
    const { API_URL } = env;

    const body = await req.json();
    const response = await nvp.post(`${API_URL}/products`, {
      body,
    });
    return NextResponse.json(response);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

export { GET, POST };
