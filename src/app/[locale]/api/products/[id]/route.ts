import { NextRequest, NextResponse } from 'next/server';
import { env } from '@/env.mjs';
import { ErrorWithStatus, nvp } from '@/libs/fetch';
import { createResponseError } from '@/utils/route';

const DELETE = async (
  _: NextRequest,
  { params }: { params: { id: string } },
) => {
  try {
    const id = params.id;
    const { API_URL } = env;
    const response = await nvp.delete(`${API_URL}/products/${id}`);
    return NextResponse.json(response);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

const PUT = async (
  req: NextRequest,
  { params }: { params: { id: string } },
) => {
  try {
    const { API_URL } = env;
    const id = params.id;
    const body = await req.json();
    const response = await nvp.put(`${API_URL}/products/${id}`, {
      body,
    });
    return NextResponse.json(response);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

export { DELETE, PUT };
