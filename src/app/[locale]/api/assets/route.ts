import { ErrorWithStatus } from '@/libs/fetch';
import { createResponseError } from '@/utils/route';
import { put } from '@vercel/blob';
import { NextResponse } from 'next/server';

export const POST = async (request: Request) => {
  try {
    const formData = await request.formData();
    const file = formData.get('file');
    const timestamp = Date.now();
    const filename = `file_${timestamp}.jpg`;

    if (file) {
      const blob = await put(`products/${filename}`, file, {
        access: 'public',
      });

      return NextResponse.json(blob);
    } else {
      throw new Error('No file provided');
    }
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};
