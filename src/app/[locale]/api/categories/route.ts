import { NextRequest, NextResponse } from 'next/server';
import { env } from '@/env.mjs';
import { ErrorWithStatus, nvp } from '@/libs/fetch';
import { createResponseError } from '@/utils/route';

const GET = async (_: NextRequest) => {
  try {
    const { API_URL } = env;
    const data = await nvp.get(`${API_URL}/categories`);
    return NextResponse.json(data);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

export { GET };
