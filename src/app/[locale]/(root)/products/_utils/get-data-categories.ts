import { ErrorWithStatus, nvp } from '@/libs/fetch';
import { Category } from '../_schemas/category';
import { CategoryResponse } from '@/app/[locale]/api/categories/_schema/response';

interface DataReturn {
  records?: Category[];
  error?: { status: number; message: string };
}

const getData = async (): Promise<DataReturn> => {
  const records: Category[] = [];
  try {
    const response: CategoryResponse[] = await nvp.get('/api/categories');

    const validatedData = response;
    validatedData.map((item: CategoryResponse) => {
      return records.push({
        id: item?.id ?? -1,
        name: item.name ?? '',
      });
    });

    return {
      records,
    };
  } catch (error) {
    const err = error as ErrorWithStatus;

    return {
      error: { status: err.status, message: err.error },
    };
  }
};

export { getData };
