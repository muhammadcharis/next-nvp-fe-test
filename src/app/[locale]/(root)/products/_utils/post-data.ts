import { ErrorWithStatus, nvp } from '@/libs/fetch';
import { Product } from '../_schemas/product';

export interface PostOption {
  id: number | undefined;
  payload: Omit<Product, 'id' | 'no' | 'categoryName'> | undefined;
}

interface DataReturn {
  success?: boolean;
  error?: { status: number; message: string };
}

const postData = async ({ id, payload }: PostOption): Promise<DataReturn> => {
  try {
    if (id) {
      await nvp.put(`/api/products/${id}`, { body: payload });
    } else {
      await nvp.post('/api/products', { body: payload });
    }

    return {
      success: true,
    };
  } catch (error) {
    const err = error as ErrorWithStatus;

    return {
      error: { status: err.status, message: err.error },
    };
  }
};

export { postData };
