import { ErrorWithStatus, nvp } from '@/libs/fetch';
import { Product } from '../_schemas/product';
import { PaginationParams } from '@/app/consts/pagination';
import { ProductResponse } from '@/app/[locale]/api/products/_schema/response';
import { normalizeImages } from '@/utils/images';

export interface IDataParams {
  paginationParams: PaginationParams;
  keyword?: string;
}

interface DataReturn {
  records?: Product[];
  hasData?: boolean;
  error?: { status: number; message: string };
}

const getData = async ({
  paginationParams,
  keyword,
}: IDataParams): Promise<DataReturn> => {
  const records: Product[] = [];
  try {
    const params: Record<string, string> = {};
    let url = `/api/products`;

    const limit = paginationParams.limit ?? 5;
    const offset = paginationParams.offset ?? 0;

    params.limit = limit.toString();
    params.offset = offset.toString();

    if (keyword) params.keyword = keyword;

    const searchParams = new URLSearchParams(params);
    url += `?${searchParams.toString()}`;

    const response: ProductResponse[] = await nvp.get(url);

    if (!response.length) {
      return { hasData: false };
    }
    const validatedData = response;
    validatedData.map((item: ProductResponse, index: number) => {
      const images = item?.images ?? [];
      const cleanImageUrl = normalizeImages(images);
      return records.push({
        id: item?.id ?? -1,
        categoryId: item?.category?.id ?? -1,
        categoryName: item?.category?.name ?? '',
        description: item?.description ?? '',
        images: cleanImageUrl,
        price: item?.price ?? -1,
        title: item?.title ?? '',
        no: index + 1 + offset,
      });
    });

    return {
      records,
      hasData: true,
    };
  } catch (error) {
    const err = error as ErrorWithStatus;

    return {
      error: { status: err.status, message: err.error },
    };
  }
};

export { getData };
