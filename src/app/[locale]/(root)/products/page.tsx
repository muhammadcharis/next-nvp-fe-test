import { Header } from '@/app/components/header';
import { getTranslations } from 'next-intl/server';
import { Table } from './_components/table';
import { ActionButton } from './_components/action-button';

interface ProductPageProps {
  params: { locale: string };
}

export const generateMetadata = async (props: ProductPageProps) => {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'products',
  });

  return {
    title: t('metaTitle'),
    description: t('metaDescription'),
  };
};

const Page = async (props: ProductPageProps) => {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'products',
  });
  return (
    <>
      <div className="flex flex-col justify-start items-start mb-7">
        <Header
          title={t('title')}
          description={t('pageDescription')}
          actionButton={<ActionButton />}
        />
      </div>
      <Table />
    </>
  );
};

export default Page;
