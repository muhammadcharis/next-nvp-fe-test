import { z } from 'zod';

export const ProductSchema = z.object({
  title: z.string().min(1, { message: 'products.form.error.title.required' }),
  price: z
    .number()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value !== 0;
      },
      {
        message: 'products.form.error.price.required',
      },
    ),
  description: z
    .string()
    .min(1, { message: 'products.form.error.description.required' }),
  categoryId: z
    .number()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value !== 0;
      },
      {
        message: 'products.form.error.categoryId.required',
      },
    ),
  images: z
    .array(z.string().url())
    .min(1, { message: 'products.form.error.images.required' }),
});

export type Product = z.infer<typeof ProductSchema> & {
  id: number | undefined;
  no: number;
  categoryName: string;
};
