'use client';
import { Button } from '@/app/components/ui/button';
import { setHasModal } from '@/app/redux/slices/productSlices';
import { useTranslations } from 'next-intl';
import { useDispatch } from 'react-redux';

const ActionButton = () => {
  const t = useTranslations('products');
  const dispatch = useDispatch();
  return (
    <Button
      type="primary"
      variant="default"
      buttonSize={'lg'}
      onClick={() => dispatch(setHasModal('create'))}
    >
      {t('header.add')}
    </Button>
  );
};

export { ActionButton };
