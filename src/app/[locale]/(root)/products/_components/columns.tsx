'use client';

import type { ColumnDef } from '@tanstack/react-table';

import { Product } from '../_schemas/product';
import { Header } from './table';
import { Img } from '@/app/components/ui/image';
import { Handle, TableRowAction } from '@/app/components/ui/table-row-action';

interface ColumnParams {
  header: Header;
  onHandleCart: (user: Product) => void;
  onHandleEdit: (user: Product) => void;
  onHandleDelete: (user: Product) => void;
}

export const createColumns = ({
  header,
  onHandleCart,
  onHandleEdit,
  onHandleDelete,
}: ColumnParams): ColumnDef<Product>[] => {
  return [
    {
      accessorKey: 'no',
      meta: 'text',
      header: header.no,
    },
    {
      accessorKey: 'title',
      meta: 'text',
      header: header.title,
    },
    {
      accessorKey: 'price',
      meta: 'text',
      header: header.price,
    },
    {
      accessorKey: 'categoryName',
      meta: 'text',
      header: header.category,
    },
    {
      accessorKey: 'images',
      header: header.image,
      meta: 'image',
      cell: ({ row }) => {
        const label = row.getValue('title') as string;
        const fallbackSrc = '/images/Image_not_available.png';
        const images = row.getValue('images') as string[];
        const imageUrl = images.length > 0 ? images[0] : fallbackSrc;

        return (
          <Img
            src={imageUrl!}
            alt={label}
            width={130}
            height={87}
            fallbackSrc={fallbackSrc}
          />
        );
      },
    },
    {
      id: 'actions',
      meta: 'button',
      cell: ({ row }) => {
        const handles: Handle[] = [
          {
            name: 'cart',
            callback: () => onHandleCart(row.original),
          },
          {
            name: 'update',
            callback: () => onHandleEdit(row.original),
          },
          {
            name: 'delete',
            callback: () => onHandleDelete(row.original),
          },
        ];

        return <TableRowAction handles={handles} row={row.original} />;
      },
    },
  ];
};
