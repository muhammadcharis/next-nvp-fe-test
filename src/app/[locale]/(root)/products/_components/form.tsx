'use client';

import React, { useEffect, useMemo, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Product, ProductSchema } from '../_schemas/product';
import { zodResolver } from '@hookform/resolvers/zod';
import { Form as AntdForm, Upload, UploadFile, message } from 'antd';
import { Button } from '@/app/components/ui/button';
import { MessageKeys, useTranslations } from 'next-intl';
import { UploadOutlined } from '@ant-design/icons';

import { FormLabel } from './form-label';
import { Input } from '@/app/components/ui/input';
import { TextArea } from '@/app/components/ui/text-area';
import { Select, Option } from '@/app/components/ui/select';
import { useGetCategories } from '../_hooks/use-get-categories';
import { nvp } from '@/libs/fetch';
import { usePostProduct } from '../_hooks/use-post-products';

interface FormProps {
  initialData: Product | null;
  onHandleCancel: () => void;
  onHandleSuccess: () => void;
}

const Form = ({ onHandleCancel, onHandleSuccess, initialData }: FormProps) => {
  const t = useTranslations();
  const { data: categories, isLoading: isFetching } = useGetCategories();
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [fileUrls, setFileUrls] = useState<string[]>([]);
  const [currentUrl, setCurrentUrl] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { mutate, isSuccess } = usePostProduct({
    id: undefined,
    payload: undefined,
  });

  const defaultValues: Pick<
    Product,
    'categoryId' | 'description' | 'images' | 'id' | 'price' | 'title'
  > = useMemo(
    () => ({
      categoryId: undefined,
      description: '',
      images: [],
      id: undefined,
      price: undefined,
      title: '',
    }),
    [],
  );

  const {
    handleSubmit,
    control,
    formState: { errors },
    setValue,
    reset,
  } = useForm<Product>({
    resolver: zodResolver(ProductSchema),
    defaultValues,
  });

  const submitForm = async (data: Product) => {
    setIsLoading(true);
    try {
      const uploadPromises: Promise<string | null>[] = fileList
        .filter((file) => !file.uid.includes('done'))
        .map(async (file) => {
          if (!file) return Promise.resolve(null);

          const formData = new FormData();
          if (file.originFileObj) {
            formData.append('file', file.originFileObj);
            try {
              const result = await nvp.post(`/api/assets`, { body: formData });
              return result ? result.url : null;
            } catch (error) {
              console.error('Error uploading file:', error);
              return null;
            }
          } else {
            return Promise.resolve(null);
          }
        });

      const results = (await Promise.all(uploadPromises))
        .filter((result): result is string => result !== null)
        .map((result) => result);

      const payload: Omit<Product, 'id' | 'no' | 'categoryName'> = {
        categoryId: data.categoryId,
        description: data.description,
        images: currentUrl.concat(results),
        title: data.title,
        price: data.price,
      };

      mutate({ id: initialData?.id, payload });
    } catch (error) {
      console.error(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const newFileUrls: string[] = [];
    fileList.forEach((file) => {
      if (file.originFileObj) {
        const reader = new FileReader();
        reader.onload = () => {
          const preview = reader.result;
          if (typeof preview === 'string') {
            newFileUrls.push(preview);
          }
          if (newFileUrls.length === fileList.length) {
            setFileUrls([...newFileUrls]);
            setValue('images', newFileUrls); // Update images value in the form
          }
        };
        reader.readAsDataURL(file.originFileObj);
      }
    });
  }, [fileList, setValue]);

  useEffect(() => {
    if (initialData) {
      const initialFileList: UploadFile[] = initialData.images.map(
        (url, index) => ({
          uid: `done-${index}`,
          name: url.split('/').pop()!,
          status: 'done',
          url,
        }),
      );

      setFileList(initialFileList);
      setFileUrls(initialData.images);
      setCurrentUrl(initialData.images);

      reset({
        categoryId: initialData.categoryId,
        description: initialData.description,
        price: initialData.price!,
        id: initialData.id,
        images: initialData.images,
        title: initialData.title,
      });
    } else {
      reset({});
      setFileList([]);
      setFileUrls([]);
      setCurrentUrl([]);
    }
  }, [initialData]);

  useEffect(() => {
    if (isSuccess) {
      setIsLoading(false);
      onHandleSuccess();
    }
  }, [isSuccess]);

  return (
    <AntdForm
      onFinish={handleSubmit(submitForm)}
      layout="vertical"
      style={{ marginTop: 10 }}
    >
      <AntdForm.Item
        colon={false}
        style={{ marginBottom: 8 }}
        label={<FormLabel label="products.form.label.title" />}
        validateStatus={errors.title ? 'error' : ''}
        help={
          errors.title
            ? t(errors.title.message as MessageKeys<IntlMessages, 'global'>)
            : null
        }
      >
        <Controller
          name="title"
          control={control}
          render={({ field }) => (
            <Input
              {...field}
              placeholder={t('products.form.placeholder.title')}
            />
          )}
        />
      </AntdForm.Item>
      <AntdForm.Item
        colon={false}
        style={{ marginBottom: 8 }}
        label={<FormLabel label="products.form.label.category" />}
        validateStatus={errors.categoryId ? 'error' : ''}
        help={
          errors.categoryId
            ? t(
                errors.categoryId.message as MessageKeys<
                  IntlMessages,
                  'global'
                >,
              )
            : null
        }
      >
        <Controller
          name="categoryId"
          control={control}
          render={({ field }) => {
            return (
              <Select
                key={'categoryOption'}
                placeholder={t('products.form.placeholder.category')}
                {...field}
                loading={isFetching}
                style={{
                  height: 38,
                }}
              >
                {categories?.records?.map(
                  (item) =>
                    (
                      <Option key={item.id} value={item.id}>
                        {item.name}
                      </Option>
                    ) ?? [],
                )}
              </Select>
            );
          }}
        />
      </AntdForm.Item>
      <AntdForm.Item
        colon={false}
        style={{ marginBottom: 8 }}
        label={<FormLabel label="products.form.label.price" />}
        validateStatus={errors.price ? 'error' : ''}
        help={
          errors.price
            ? t(errors.price.message as MessageKeys<IntlMessages, 'global'>)
            : null
        }
      >
        <Controller
          name="price"
          control={control}
          render={({ field }) => (
            <Input
              {...field}
              type="number"
              placeholder={t('products.form.placeholder.price')}
              onChange={(e) => {
                const value = e.target.value;
                if (value) {
                  field.onChange(parseFloat(value));
                } else field.onChange(undefined);
              }}
            />
          )}
        />
      </AntdForm.Item>
      <AntdForm.Item
        colon={false}
        style={{ marginBottom: 8 }}
        label={<FormLabel label="products.form.label.description" />}
        validateStatus={errors.description ? 'error' : ''}
        help={
          errors.description
            ? t(
                errors.description.message as MessageKeys<
                  IntlMessages,
                  'global'
                >,
              )
            : null
        }
      >
        <Controller
          name="description"
          control={control}
          render={({ field }) => (
            <TextArea
              {...field}
              placeholder={t('products.form.placeholder.description')}
            />
          )}
        />
      </AntdForm.Item>
      <AntdForm.Item
        colon={false}
        label={<FormLabel label="products.form.label.images" />}
        validateStatus={errors.images ? 'error' : ''}
        help={
          errors.images
            ? t(errors.images.message as MessageKeys<IntlMessages, 'global'>)
            : null
        }
      >
        <Controller
          name="images"
          control={control}
          render={({ field }) => (
            <Upload.Dragger
              className={errors.images ? 'upload-dragger-error' : ''}
              name="images"
              listType="picture"
              multiple
              fileList={fileList}
              beforeUpload={(file) => {
                const isJpgOrPng =
                  file.type === 'image/jpeg' || file.type === 'image/png';
                const isLt2M = file.size / 1024 / 1024 < 2;
                if (!isJpgOrPng) {
                  message.error(t('products.form.error.images.format'));
                }
                if (!isLt2M) {
                  message.error(t('products.form.error.images.size'));
                }
                return isJpgOrPng || Upload.LIST_IGNORE;
              }}
              onChange={(info) => {
                const { fileList } = info;
                setFileList(fileList);
              }}
              onRemove={(file) => {
                const newFileList: UploadFile[] = fileList.filter(
                  (item) => item.uid !== file.uid,
                );

                // Remove the corresponding file URL from fileUrls
                const newFileUrls = fileUrls.filter(
                  (_, index) =>
                    index !==
                    fileList.findIndex((item) => item.uid === file.uid),
                );

                setFileList(newFileList);
                setFileUrls(newFileUrls);

                field.onChange(newFileUrls);
              }}
              accept=".jpg,.png"
            >
              <p className="ant-upload-drag-icon">
                <UploadOutlined className="!text-black" />
              </p>
              <p className="ant-upload-text">
                {t('products.form.placeholder.images.label')}
              </p>
              <p className="ant-upload-hint">
                {t('products.form.placeholder.images.help')}
              </p>
            </Upload.Dragger>
          )}
        />
      </AntdForm.Item>
      <div className="justify-end !mt-10 items-end flex gap-3">
        <Button variant={'outline'} onClick={onHandleCancel}>
          {t('global.cancel')}
        </Button>
        <Button
          type="primary"
          variant={'default'}
          htmlType="submit"
          loading={isLoading}
        >
          {t('global.save')}
        </Button>
      </div>
    </AntdForm>
  );
};

export { Form };
