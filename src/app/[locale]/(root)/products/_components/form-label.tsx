import { MessageKeys, useTranslations } from 'next-intl';

interface FormLabelProps {
  label: string;
}
const FormLabel = ({ label }: FormLabelProps) => {
  const t = useTranslations();
  return (
    <span className="font-medium">
      {t(label as MessageKeys<IntlMessages, 'global'>)}
      <span className="text-red-500">*</span>
    </span>
  );
};

export { FormLabel };
