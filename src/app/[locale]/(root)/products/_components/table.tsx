'use client';

import { useTranslations } from 'next-intl';
import { Product } from '../_schemas/product';
import { DataTable } from '@/app/components/datatable';
import { createColumns } from './columns';
import { useGetProducts } from '../_hooks/use-get-products';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/app/redux/store';
import {
  setLimit,
  setOffset,
  setSelectedProduct,
  setHasModal,
} from '@/app/redux/slices/productSlices';
import { PopConfirmation } from '@/app/components/pop-confirmation';
import { useQueryClient } from '@tanstack/react-query';
import { notification } from 'antd';
import { DialogForm } from '@/app/components/dialog-form';
import { Form } from './form';
import { addCart } from '@/app/redux/slices/cartSlices';

export type Header = {
  no: string;
  title: string;
  price: string;
  category: string;
  image: string;
};

const Table = () => {
  const t = useTranslations();
  const dispatch = useDispatch();
  const queryClient = useQueryClient();
  const limit = useSelector((state: RootState) => state.products.limit);
  const offset = useSelector((state: RootState) => state.products.offset);
  const selectedData = useSelector(
    (state: RootState) => state.products.selectedProduct,
  );

  const hasModal = useSelector((state: RootState) => state.products.hasModal);
  const isModalDeleteOpen = hasModal === 'delete';
  const isModalCreateOpen = hasModal === 'create';
  const isModalEditOpen = hasModal === 'edit';

  const { data, isLoading: isFetching } = useGetProducts({
    paginationParams: {
      limit: limit,
      offset: offset,
    },
  });

  const handleEdit = (data: Product) => {
    dispatch(setSelectedProduct(data));
    dispatch(setHasModal('edit'));
  };

  const handleCart = (data: Product) => {
    dispatch(addCart(data));
    notification.success({
      message: t('carts.notification.add.title'),
      description: t('carts.notification.add.description'),
    });
  };

  const handleDelete = (data: Product) => {
    dispatch(setSelectedProduct(data));
    dispatch(setHasModal('delete'));
  };

  const handleClosePopConfirm = () => {
    dispatch(setSelectedProduct(null));
    dispatch(setHasModal(null));
  };

  const handleOnSuccess = ({
    title,
    message,
  }: {
    title: string;
    message: string;
  }) => {
    notification.success({
      message: title,
      description: message,
    });
    queryClient.invalidateQueries({ queryKey: ['products'] });
    dispatch(setSelectedProduct(null));
    dispatch(setHasModal(null));
  };

  const header: Header = {
    no: t('products.table.no'),
    title: t('products.table.title'),
    price: t('products.table.price'),
    category: t('products.table.category'),
    image: t('products.table.images'),
  };

  return (
    <>
      <DataTable
        columns={createColumns({
          header,
          onHandleCart: handleCart,
          onHandleEdit: handleEdit,
          onHandleDelete: handleDelete,
        })}
        isLoading={isFetching}
        data={data?.records ?? []}
        hasData={data?.hasData ?? false}
        hasPagination
        limit={limit}
        onHandleSetPage={(newIndex: number) => {
          const newOffset = newIndex * limit;
          dispatch(setOffset(newOffset));
        }}
        onHandleSetLimit={(newLimit: number) => {
          dispatch(setLimit(newLimit));
        }}
      />

      <PopConfirmation
        urlDelete={`/api/products/${selectedData?.id}`}
        isOpen={isModalDeleteOpen}
        title={t('products.form.label.header.delete')}
        description={t('global.deleteDescription', {
          name: selectedData?.title,
        })}
        onHandleCancel={handleClosePopConfirm}
        onHandleSuccess={() => {
          handleOnSuccess({
            title: t('products.notification.delete.title'),
            message: t('products.notification.delete.description'),
          });
        }}
      />

      <DialogForm
        onHandleCancel={handleClosePopConfirm}
        isOpen={isModalCreateOpen || isModalEditOpen}
        title={
          isModalCreateOpen
            ? t('products.form.label.header.add')
            : t('products.form.label.header.edit')
        }
      >
        <Form
          initialData={selectedData}
          onHandleCancel={handleClosePopConfirm}
          onHandleSuccess={() => {
            handleOnSuccess({
              title: isModalCreateOpen
                ? t('products.notification.insert.title')
                : t('products.notification.update.title'),
              message: isModalCreateOpen
                ? t('products.notification.insert.description')
                : t('products.notification.update.description'),
            });
          }}
        />
      </DialogForm>
    </>
  );
};

export { Table };
