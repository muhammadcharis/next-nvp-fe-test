import { useQuery } from '@tanstack/react-query';
import { getData } from '../_utils/get-data-categories';

const useGetCategories = () => {
  return useQuery({
    queryKey: ['categories'],
    queryFn: () => getData(),
    refetchInterval: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
    refetchOnMount: false,
    retry: false,
  });
};

export { useGetCategories };
