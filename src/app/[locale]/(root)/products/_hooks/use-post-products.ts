import { useMutation } from '@tanstack/react-query';
import { PostOption, postData } from '../_utils/post-data';

const usePostProduct = ({ id, payload }: PostOption) => {
  return useMutation({
    mutationKey: ['products', id, payload],
    mutationFn: ({ id, payload }: PostOption) => {
      return postData({ id, payload });
    },
  });
};

export { usePostProduct };
