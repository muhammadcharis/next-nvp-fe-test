import { PaginationParams } from '@/app/consts/pagination';
import { useQuery } from '@tanstack/react-query';
import { getData } from '../_utils/get-data';

interface GetProductsOptions {
  paginationParams: PaginationParams;
  keyword?: string;
}

const useGetProducts = ({ paginationParams, keyword }: GetProductsOptions) => {
  return useQuery({
    queryKey: [
      'products',
      paginationParams.offset,
      keyword,
      paginationParams.limit,
    ],
    queryFn: () =>
      getData({
        paginationParams,
        keyword,
      }),
    refetchInterval: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
    refetchOnMount: false,
    retry: false,
  });
};

export { useGetProducts };
