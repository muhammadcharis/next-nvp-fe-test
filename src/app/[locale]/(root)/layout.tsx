import React from 'react';
import { Sidebar } from '@/app/components/sidebar';
import { Navbar } from '@/app/components/navbar';
import { AppProviders } from './_components/app-provider';

interface AppLayoutProps {
  children: Readonly<React.ReactNode>;
}

const AppLayout = ({ children }: AppLayoutProps) => {
  return (
    <AppProviders>
      <div
        className="flex min-h-svh w-full bg-white"
        style={
          {
            '--expandedSidebarWidth': '250px',
          } as React.CSSProperties
        }
      >
        <Sidebar />
        <div className="flex flex-1 flex-col w-full md:ml-[var(--expandedSidebarWidth)] md:w-[calc(100%-var(--expandedSidebarWidth))]">
          <Navbar />
          <main className="flex flex-1 flex-col gap-4 bg-layout p-5 lg:gap-6 lg:p-6 !bg-white">
            {children}
          </main>
        </div>
      </div>
    </AppProviders>
  );
};

export default AppLayout;
