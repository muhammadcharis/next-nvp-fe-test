import { Header } from '@/app/components/header';
import { getTranslations } from 'next-intl/server';
import { List } from './_components/list';

interface CartPageProps {
  params: { locale: string };
}

export const generateMetadata = async (props: CartPageProps) => {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'carts',
  });

  return {
    title: t('metaTitle'),
    description: t('metaDescription'),
  };
};

const Page = async (props: CartPageProps) => {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'carts',
  });
  return (
    <>
      <div className="flex flex-col justify-start items-start">
        <Header title={t('title')} description={t('pageDescription')} />
      </div>
      <List />
    </>
  );
};

export default Page;
