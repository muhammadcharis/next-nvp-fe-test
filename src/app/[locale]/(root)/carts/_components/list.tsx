'use client';

import { DeleteOutlined } from '@ant-design/icons';
import { List as AntdList, Image as AntdImg, Empty } from 'antd';
import React, { useEffect, useState } from 'react';
import VirtualList from 'rc-virtual-list';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/app/redux/store';
import { removeCart } from '@/app/redux/slices/cartSlices';
import { Button } from '@/app/components/ui/button';
import { useTranslations } from 'next-intl';
import { Product } from '../../products/_schemas/product';

const ContainerHeight = 700;

const List = () => {
  const t = useTranslations();
  const carts = useSelector((state: RootState) => state.carts);
  const [cartList, setCartList] = useState<Product[]>([]);
  const dispatch = useDispatch();

  useEffect(() => {
    setCartList(carts.items);
  }, [carts]);

  if (!cartList.length)
    return (
      <div className="flex justify-center items-center min-h-[500px] ">
        <Empty description={t('carts.empty')} />
      </div>
    );

  return (
    <AntdList itemLayout="vertical" size="large">
      <VirtualList
        data={cartList}
        height={ContainerHeight}
        itemHeight={47}
        itemKey="email"
      >
        {(item) => {
          return (
            <AntdList.Item
              style={{ paddingLeft: 0, paddingRight: 0 }}
              key={item.id}
              actions={[
                <Button
                  variant={'destructive'}
                  block
                  danger
                  onClick={() => {
                    dispatch(removeCart(item.id!));
                  }}
                >
                  <DeleteOutlined />
                  {t('global.delete')}
                </Button>,
              ]}
            >
              <AntdList.Item.Meta
                title={
                  <p className="font-bold first-letter:uppercase">
                    {item.title}
                  </p>
                }
                description={item.description}
              />
              <AntdImg.PreviewGroup>
                <div className="flex flex-col sm:flex-row gap-3">
                  {item.images.map((image, index) => {
                    return (
                      <AntdImg
                        key={index}
                        alt={item.title}
                        src={image!}
                        height={250}
                        className="rounded-lg overflow-hidden"
                      />
                    );
                  })}
                </div>
              </AntdImg.PreviewGroup>
            </AntdList.Item>
          );
        }}
      </VirtualList>
    </AntdList>
  );
};

export { List };
