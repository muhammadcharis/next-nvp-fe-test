import type { MetadataRoute } from 'next';

import { env } from '@/env.mjs';

const sitemap = (): MetadataRoute.Sitemap => {
  return [
    {
      url: `${env.NEXT_PUBLIC_APP_URL}/`,
      lastModified: new Date(),
      changeFrequency: 'daily',
      priority: 0.7,
    },
    // Add more URLs here
  ];
};

export default sitemap;
