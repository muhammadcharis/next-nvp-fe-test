export type RouteName = 'products' | 'carts';

export interface Route {
  name: RouteName;
  href: string;
  hasCart: boolean;
}

export const ROUTES: Route[] = [
  {
    name: 'products',
    href: '/products',
    hasCart: false,
  },
  {
    name: 'carts',
    href: '/carts',
    hasCart: true,
  },
];
