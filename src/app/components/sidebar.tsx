import { MenuItem } from './menu-item';

const Sidebar = () => {
  return (
    <div className="md:block hidden w-full md:w-[var(--expandedSidebarWidth)] p-3 border-r  fixed min-h-svh">
      <MenuItem />
    </div>
  );
};

export { Sidebar };
