import { ReactNode } from 'react';

interface HeaderProps {
  title: string;
  description: string;
  actionButton?: ReactNode;
}
const Header = ({ title, description, actionButton }: HeaderProps) => {
  return (
    <>
      <div className="w-full flex justify-between items-center gap-2 mb-3">
        <div className="flex flex-col gap-1">
          <h1 className="font-bold text-2xl tracking-tighter">{title}</h1>
          <p className="text-muted-foreground text-sm">{description}</p>
        </div>
        {actionButton && <div className="ml-auto">{actionButton}</div>}
      </div>
    </>
  );
};

export { Header };
