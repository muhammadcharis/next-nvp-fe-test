'use client';
import { Drawer, Select } from 'antd';
import { usePathname, useRouter } from 'next/navigation';
import { Button } from './ui/button';
import { MenuFoldOutlined } from '@ant-design/icons';
import { useLocale } from 'next-intl';
import { appConfig } from '@/app.config';
import { useEffect, useState } from 'react';
import { MenuItem } from './menu-item';

const Navbar = () => {
  const router = useRouter();
  const pathname = usePathname();
  const locale = useLocale();
  const [selectedLang, setSelectedLang] = useState<string>(locale);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const currentLang = pathname.split('/')[1] ?? '';
    if (!appConfig.i18n.locales.includes(currentLang)) {
      router.push(`/${selectedLang}${pathname}`);
    } else {
      const newPathname = pathname.replace(
        `/${currentLang}`,
        `/${selectedLang}`,
      );
      router.push(newPathname);
    }
    router.refresh();
  }, [selectedLang]);

  return (
    <>
      <div className="border-b">
        <div className="h-16 flex items-center px-4 justify-between">
          <Button
            onClick={() => setOpen(true)}
            className="md:!hidden block bg-red-200"
            buttonSize={'icon'}
            variant={'ghost'}
            icon={<MenuFoldOutlined />}
          />
          <Select
            defaultValue={locale}
            style={{ width: 130, height: 38 }}
            onChange={(value) => {
              setSelectedLang(value);
            }}
          >
            {appConfig.locales.map((val) => {
              return (
                <Select.Option key={val.value} value={val.value}>
                  {val.label}
                </Select.Option>
              );
            })}
          </Select>
        </div>
      </div>
      <Drawer
        title="Menu"
        placement="left"
        closable={true}
        onClose={() => setOpen(false)}
        open={open}
        key="left"
      >
        <MenuItem onHandleClose={() => setOpen(false)} />
      </Drawer>
    </>
  );
};

export { Navbar };
