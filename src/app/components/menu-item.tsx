'use client';
import { usePathname, useRouter } from 'next/navigation';
import { ShoppingCartOutlined, ProductOutlined } from '@ant-design/icons';
import { cn } from '@/utils/cn';
import { removeLocalePrefix } from '@/utils/text';
import { ROUTES, Route, RouteName } from '@/app/consts/routes';
import { useTranslations } from 'next-intl';
import { Tag } from 'antd';
import { useSelector } from 'react-redux';
import { RootState } from '@/app/redux/store';
import { useEffect, useState } from 'react';

type IconType = {
  [key in RouteName]: JSX.Element;
};

const Icon: IconType = {
  carts: <ShoppingCartOutlined className="mr-3 shrink-0" />,
  products: <ProductOutlined className="mr-3 shrink-0" />,
};

interface MenuItemProps {
  onHandleClose?: () => void;
}

const MenuItem = ({ onHandleClose }: MenuItemProps) => {
  const t = useTranslations('global');
  const pathname = usePathname();
  const router = useRouter();
  const path = removeLocalePrefix(pathname);
  const cart = useSelector((state: RootState) => state.carts.items);
  const [cartCount, setCartCount] = useState(0);

  useEffect(() => {
    setCartCount(cart.length);
  }, [cart]);

  const handleLink = (href: string) => {
    router.push(href);
    onHandleClose?.();
  };

  return (
    <>
      {ROUTES.map((menu: Route) => {
        return (
          <div
            key={menu.href}
            onClick={() => handleLink(menu.href)}
            className={cn(
              'flex items-center p-2 text-secondary-light no-underline hover:bg-accent ',
              path === menu.href
                ? 'text-black font-semibold'
                : 'text-slate-600',
            )}
          >
            <div className="w-full flex ">
              {Icon[menu.name]}
              <div className=" w-full justify-between flex">
                <span className="h-5">{t(`route.${menu.name}`)}</span>
                {menu.hasCart && <Tag color="green">{cartCount}</Tag>}
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export { MenuItem };
