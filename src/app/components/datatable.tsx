'use client';

import {
  type ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { Skeleton } from 'antd';

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/app/components/ui/table';
import type { Table as TblReact } from '@tanstack/react-table';
import { useState } from 'react';
import { Pagination } from './ui/pagination';

interface DataTableProps<TData, TValue> {
  columns: ColumnDef<TData, TValue>[];
  data: TData[];
  limit?: number;
  hasPagination?: boolean;
  hasData?: boolean;
  isLoading: boolean;
  onHandleSetLimit: (index: number) => void;
  onHandleSetPage: (page: number) => void;
}

const createSkeletonRows = <TData, TValue>(
  columns: ColumnDef<TData, TValue>[],
) => (
  <TableRow key="skeleton-row">
    {columns.map((column, index) => {
      if (column.meta === 'image') {
        return (
          <TableCell key={`${column.id}-${index}`} className="px-6 py-4">
            <Skeleton.Image active />
          </TableCell>
        );
      }
      if (column.meta === 'button') {
        return (
          <TableCell key={`${column.id}-${index}`} className="px-6 py-4">
            <Skeleton.Button active />
          </TableCell>
        );
      }
      return (
        <TableCell key={`${column.id}-${index}`} className="px-6 py-4">
          <Skeleton.Input active />
        </TableCell>
      );
    })}
  </TableRow>
);

const createTableRows = <TData,>(table: TblReact<TData>, colSpan: number) => {
  if (!table.getRowModel().rows.length) {
    return (
      <TableRow
        key={'empty-row'}
        className="text-sm font-normal leading-5.5 text-secondary-dark hover:bg-stroke/25 data-[state=selected]:bg-stroke/25"
        data-state={undefined}
      >
        <TableCell colSpan={colSpan} className="px-6 py-4 text-center">
          Data not found
        </TableCell>
      </TableRow>
    );
  }
  return table.getRowModel().rows.map((row) => (
    <TableRow
      key={row.id}
      className="text-sm font-normal leading-5.5 text-secondary-dark hover:bg-stroke/25 data-[state=selected]:bg-stroke/25"
      data-state={row.getIsSelected() ? 'selected' : undefined}
    >
      {row.getVisibleCells().map((cell) => (
        <TableCell
          key={cell.id}
          className="px-6 py-4"
          style={{
            width: cell.column.getSize(),
          }}
        >
          {flexRender(cell.column.columnDef.cell, cell.getContext())}
        </TableCell>
      ))}
    </TableRow>
  ));
};

export const DataTable = <TData, TValue>({
  columns,
  data,
  limit = 5,
  hasPagination = false,
  hasData = true,
  isLoading = false,
  onHandleSetPage,
  onHandleSetLimit,
}: DataTableProps<TData, TValue>) => {
  const [currentPage, setCurrentPage] = useState<number>(0);

  const table = useReactTable({
    data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    manualPagination: true, // turn off client-side pagination
    autoResetPageIndex: false,
    defaultColumn: {
      size: 150, // starting column size
      minSize: 20, // enforced during column resizing
      maxSize: Number.MAX_SAFE_INTEGER, // enforced during column resizing
    },
    state: {
      pagination: { pageIndex: currentPage, pageSize: limit },
    },
    onPaginationChange: (updater) => {
      const currentState = table.getState().pagination;
      const newState =
        typeof updater === 'function'
          ? updater(table.getState().pagination)
          : updater;

      const newPageIndex = newState.pageIndex;
      const newPageSize = newState.pageSize;

      if (newPageIndex !== currentState.pageIndex) {
        onHandleSetPage(newPageIndex);
        setCurrentPage(newPageIndex);
      }

      if (newPageSize !== currentState.pageSize) {
        setCurrentPage(0);
        onHandleSetLimit(newPageSize);
      }
    },
  });

  return (
    <div className="flex flex-col gap-2 rounded-lg border-l border-r border-b border-stroke bg-primary-light">
      <div className="overflow-y-auto max-h-[400px] md:max-h-[700px] rounded-md  border-t  border-stroke">
        <Table
          style={{
            width:
              table.options.columns.find((col) => col.size) &&
              table.getCenterTotalSize(),
          }}
        >
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead
                      key={header.id}
                      className="px-6 py-3 text-sm font-semibold leading-5.5 text-secondary-dark bg-white"
                      style={{
                        width: header.getSize(),
                      }}
                    >
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext(),
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {isLoading
              ? createSkeletonRows(columns)
              : createTableRows(table, columns.length)}
          </TableBody>
        </Table>
      </div>

      {hasPagination && (
        <Pagination table={table} page={currentPage} hasData={hasData} />
      )}
    </div>
  );
};
