'use client';

import { Modal } from 'antd';

interface DialogFormProps {
  children: React.ReactNode;
  title: string;
  isOpen: boolean;
  onHandleCancel: () => void;
}
const DialogForm = ({
  title,
  isOpen = false,
  children,
  onHandleCancel,
}: DialogFormProps) => {
  return (
    <Modal
      title={title}
      centered
      destroyOnClose
      maskClosable={false}
      open={isOpen}
      onCancel={onHandleCancel}
      footer={null}
    >
      {children}
    </Modal>
  );
};

export { DialogForm };
