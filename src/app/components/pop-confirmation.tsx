'use client';

import { useDelete } from '@/hooks/use-delete';
import { Modal } from 'antd';
import { useTranslations } from 'use-intl';
import { ExclamationCircleTwoTone } from '@ant-design/icons';
import { useEffect } from 'react';

interface PopConfirmationProps {
  urlDelete: string;
  title: string;
  description: string;
  isOpen: boolean;
  onHandleSuccess: () => void;
  onHandleCancel: () => void;
}
const PopConfirmation = ({
  title,
  urlDelete,
  description,
  isOpen = false,
  onHandleSuccess,
  onHandleCancel,
}: PopConfirmationProps) => {
  const t = useTranslations('global');
  const { mutate, isPending, isSuccess } = useDelete({ url: urlDelete });

  const handleConfirm = () => {
    mutate({ url: urlDelete });
  };

  useEffect(() => {
    if (isSuccess) {
      onHandleSuccess();
    }
  }, [isSuccess]);

  return (
    <Modal
      destroyOnClose
      maskClosable={false}
      title={
        <span>
          <ExclamationCircleTwoTone
            twoToneColor="#faad14"
            style={{ marginRight: '8px' }}
          />
          {title}
        </span>
      }
      centered
      open={isOpen}
      onOk={handleConfirm}
      onCancel={onHandleCancel}
      okText={t('continue')}
      confirmLoading={isPending}
      cancelText={t('cancel')}
      okButtonProps={{
        style: { background: 'hsl(5, 74%, 42%)', color: 'hsl(0 0% 100%)' },
      }}
    >
      {description}
    </Modal>
  );
};

export { PopConfirmation };
