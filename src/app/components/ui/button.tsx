import * as React from 'react';
import { cva, type VariantProps } from 'class-variance-authority';
import { cn } from '@/utils/cn';
import { Button as AntButton } from 'antd';
import { ButtonProps as AntButtonProps } from 'antd/es/button/button';

const buttonVariants = cva(
  '!inline-flex !items-center !justify-center !whitespace-nowrap !rounded-md !text-sm !font-medium !ring-offset-background !transition-colors focus-visible:!outline-none focus-visible:!ring-1 focus-visible:!ring-ring focus-visible:!ring-offset-0 disabled:!pointer-events-none disabled:!opacity-50',
  {
    variants: {
      variant: {
        default: '!bg-slate-700 transition !text-white hover:!bg-black/90',
        destructive:
          '!bg-destructive !text-destructive-foreground hover:!bg-destructive/90',
        outline:
          '!border !border-black !bg-background hover:!bg-accent hover:!text-accent-foreground',
        secondary:
          'bg-secondary text-secondary-foreground hover:bg-secondary/80',
        ghost:
          'hover:!bg-accent hover:!text-accent-foreground hover:!border-black',
        link: 'text-primary underline-offset-4 hover:underline',
      },
      buttonSize: {
        default: '!h-10 !px-8 !py-3',
        sm: '!h-9 !rounded-md !px-3',
        lg: '!h-11 !rounded-md !px-8',
        icon: '!h-10 !w-10',
      },
    },
    defaultVariants: {
      variant: 'default',
      buttonSize: 'default', // Ukuran default untuk ikon
    },
  },
);

export interface ButtonProps extends AntButtonProps {
  isLoading?: boolean;
  buttonSize?: MergedSizeType;
  variant:
    | 'default'
    | 'destructive'
    | 'outline'
    | 'secondary'
    | 'ghost'
    | 'link';
  onClick?: () => void;
}

// Use Union type for 'size' property to merge both types
type MergedSizeType = VariantProps<typeof buttonVariants>['buttonSize'];

const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      className,
      variant,
      buttonSize,
      isLoading = false,
      type = 'default',
      onClick,
      ...props
    },
    ref,
  ) => {
    return (
      <AntButton
        className={cn(buttonVariants({ variant, buttonSize, className }))}
        ref={ref}
        loading={isLoading}
        disabled={isLoading || props.disabled}
        type={type}
        {...props}
        onClick={onClick}
      >
        {props.children}
      </AntButton>
    );
  },
);
Button.displayName = 'Button';

export { Button, buttonVariants };
