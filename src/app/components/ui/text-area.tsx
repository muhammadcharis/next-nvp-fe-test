import React, { forwardRef, Ref } from 'react';
import { Input as AntdInput } from 'antd';
import { TextAreaProps as AntdTextAreaProps } from 'antd/es/input/TextArea';

interface TextAreaProps extends AntdTextAreaProps {}

const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (props, ref: Ref<HTMLTextAreaElement>) => {
    return (
      <AntdInput.TextArea
        ref={ref}
        {...props}
        className="!resize-none focus:!border-black hover:!border-black focus:!outline-none focus:!outline-offset-0 focus:!shadow-none"
      />
    );
  },
);

export { TextArea };
