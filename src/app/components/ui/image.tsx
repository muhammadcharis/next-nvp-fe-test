import { useState } from 'react';
import Image from 'next/image';

interface ImgProps {
  src: string;
  alt: string;
  width: number;
  height: number;
  fallbackSrc: string;
}

const Img = ({ src, alt, width, height, fallbackSrc }: ImgProps) => {
  const [imageSrc, setImageSrc] = useState(src);

  return (
    <Image
      alt={alt}
      height={height}
      src={imageSrc}
      width={width}
      onError={() => setImageSrc(fallbackSrc)}
    />
  );
};

export { Img };
