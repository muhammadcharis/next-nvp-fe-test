import React, { forwardRef, Ref } from 'react';
import { Select as AntdSelect } from 'antd';
import { SelectProps as AntdSelectProps, RefSelectProps } from 'antd/es/select';

interface SelectProp extends AntdSelectProps {}

const Select = forwardRef<RefSelectProps, SelectProp>(
  (props, ref: Ref<RefSelectProps>) => {
    return (
      <AntdSelect
        ref={ref}
        {...props}
        className="focus:!border-black hover:!border-black"
      />
    );
  },
);

const { Option } = AntdSelect;
export { Select, Option };
