'use client';

import { useTranslations } from 'next-intl';
import {
  EditOutlined,
  DeleteOutlined,
  DownOutlined,
  ShoppingCartOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Button } from './button';
import { Dropdown, Space } from 'antd';

export type Item = Record<
  string,
  string | number | boolean | string[] | object | undefined
>;

export interface Handle {
  name: 'cart' | 'update' | 'delete';
  callback: (item: Item) => void;
}

interface TableRowActionProps {
  row: Item;
  handles: Handle[];
}

const TableRowAction = ({ row, handles }: TableRowActionProps) => {
  const t = useTranslations('global');
  const handleItemClick = (item: Item, name: 'cart' | 'update' | 'delete') => {
    const handle = handles.find((handle) => handle.name === name);
    if (handle) {
      handle.callback(item);
    }
  };

  const actions: MenuProps = {
    items: [
      {
        key: '1',
        label: t('route.carts'),
        icon: <ShoppingCartOutlined size={16} />,
        onClick: () => handleItemClick(row, 'cart'),
      },
      {
        key: '2',
        label: t('update'),
        icon: <EditOutlined size={16} />,
        onClick: () => handleItemClick(row, 'update'),
      },
      {
        key: '3',
        label: t('delete'),
        icon: <DeleteOutlined size={16} />,
        onClick: () => handleItemClick(row, 'delete'),
      },
    ],
  };

  return (
    <Dropdown menu={actions}>
      <Button variant="outline">
        <Space>
          {t('action')}
          <DownOutlined />
        </Space>
      </Button>
    </Dropdown>
  );
};

export { TableRowAction };
