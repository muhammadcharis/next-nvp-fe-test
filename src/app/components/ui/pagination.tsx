import type { Table } from '@tanstack/react-table';

import { RightOutlined, LeftOutlined } from '@ant-design/icons';
import { Button } from './button';
import { Select } from 'antd';

interface DataTablePaginationProps<TData> {
  table: Table<TData>;
  page: number;
  hasData: boolean;
}

const Pagination = <TData,>({
  table,
  page,
  hasData,
}: DataTablePaginationProps<TData>) => {
  return (
    <div className="flex items-center justify-end p-3">
      <div className="flex items-center space-x-6 lg:space-x-8">
        <div className="flex items-center space-x-2">
          <Select
            defaultValue={table.getState().pagination.pageSize}
            style={{ width: 120, height: 38 }}
            onChange={(value) => {
              table.setPageSize(Number(value));
            }}
            options={[
              { value: 5, label: '5/page' },
              { value: 10, label: '10/page' },
              { value: 20, label: '20/page' },
              { value: 40, label: '40/page' },
              { value: 50, label: '50/page' },
              { value: 100, label: '100/page' },
            ]}
          />
        </div>
        <div className="flex items-center space-x-2">
          <Button
            buttonSize={'icon'}
            variant={'ghost'}
            className="size-8 p-0"
            disabled={page === 0}
            onClick={() => table.previousPage()}
            icon={<LeftOutlined />}
          />
          <Button
            buttonSize={'icon'}
            variant={'ghost'}
            className="size-8 p-0"
            disabled={!hasData}
            onClick={() => table.nextPage()}
            icon={<RightOutlined />}
          />
        </div>
      </div>
    </div>
  );
};

export { Pagination };
