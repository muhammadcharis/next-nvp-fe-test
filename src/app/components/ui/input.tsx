import React, { forwardRef } from 'react';
import { Input as AntdInput, InputProps, InputRef } from 'antd';

interface InputProp extends InputProps {}

const Input = forwardRef<InputRef, InputProp>((props, ref) => {
  return (
    <AntdInput
      ref={ref}
      {...props}
      className="!resize-none focus:!border-black hover:!border-black focus:!outline-none focus:!outline-offset-0 !py-2 focus:!shadow-none"
    />
  );
});

export { Input };
