import type { LocalePrefix } from 'node_modules/next-intl/dist/types/src/shared/types';

interface Locale {
  value: string;
  label: string;
}

const localePrefix: LocalePrefix = 'as-needed';
//const locales = ['en', 'id'];
const locales: Locale[] = [
  { value: 'en', label: '🇬🇧 English' },
  { value: 'id', label: '🇮🇩 Indonesia' },
];
const defaultLocale = locales[0]!.value;
const localeDetection = false;

const getLocaleValues = (locales: Locale[]) => {
  return locales.map((locale) => locale.value);
};

export const appConfig = {
  i18n: {
    locales: getLocaleValues(locales),
    defaultLocale,
    localePrefix,
    localeDetection,
  },
  locales,
};
