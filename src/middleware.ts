import type { NextRequest } from 'next/server';
import createIntlMiddleware from 'next-intl/middleware';

import { appConfig } from '@/app.config';

const middleware = async (req: NextRequest) => {
  const handleI18nRouting = createIntlMiddleware({
    locales: appConfig.i18n.locales,
    defaultLocale: appConfig.i18n.defaultLocale,
    localeDetection: appConfig.i18n.localeDetection,
    localePrefix: appConfig.i18n.localePrefix,
  });
  const response = handleI18nRouting(req);

  return response;
};

export const config = {
  matcher: ['/((?!.+\\.[\\w]+$|_next).*)', '/', '/(api|trpc)(.*)'],
};

export default middleware;
