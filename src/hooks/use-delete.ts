import { deleteData } from '@/utils/delete-data';
import { useMutation } from '@tanstack/react-query';

type DeleteOption = {
  url: string;
};

const useDelete = ({ url }: DeleteOption) => {
  return useMutation({
    mutationKey: ['deleteProduct', url],
    mutationFn: ({ url }: DeleteOption) => {
      return deleteData({ url });
    },
  });
};

export { useDelete };
